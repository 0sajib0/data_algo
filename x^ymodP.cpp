#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
LL pow(int x,int y,LL p)
{
    LL res=1;
    while(y>0)
    {
         //// If y is odd, multiply x with result
        if(y&1)
        {
            res=(res*x)%p;
        }
        y=y>>1;//y=y/2;
        x=(x*x)%p;
    }
    return res;
}
int main()
{
    int x,y,p;
    cin>>x>>y>>p;
    cout<<"x^ymodP= "<<pow(x,y,p)<<endl;
}
