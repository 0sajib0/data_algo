#include<bits/stdc++.h>
using namespace std;
int min3(int a,int b,int c)
{
    return min(min(a,b),c);
}
int editDist(string s1,string s2,int l1,int l2)
{
    // If first string is empty, the only option is to
    // insert all characters of second string into first
    if(l1==0) return l2;
    // If second string is empty, the only option is to
    // remove all characters of first string
    if(l2==0) return l1;

    // If last characters of two strings are same, nothing
    // much to do. Ignore last characters and get count for
    // remaining strings.

    if(s1[l1-1]==s2[l2-1]) return editDist(s1,s2,l1-1,l2-1);
    // If last characters are not same, consider all three
    // operations on last character of first string, recursively
    // compute minimum cost for all three operations and take
    // minimum of three values.

    //insert
    //remove
    //replace
    return 1+min3(editDist(s1,s2,l1,l2-1),editDist(s1,s2,l1-1,l2),editDist(s1,s2,l1-1,l2-1));
}
int main()
{
    string s1,s2;
    cin>>s1>>s2;
    cout<<editDist(s1,s2,s1.length(),s2.length())<<endl;
}
