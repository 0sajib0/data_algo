#include<bits/stdc++.h>
using namespace std;
int cost[100][100];
int min3(int a,int b,int c)
{
    return min(min(a,b),c);
}
int minCost(int m,int n)
{
   int t[m][n];
   int i,j;
   t[0][0]=cost[0][0];
   for(i=1;i<m;i++) t[i][0]=t[i-1][0]+cost[i][0];
   for(i=1;i<n;i++) t[0][i]=t[0][i-1]+cost[0][i];

   for(i=1;i<=m;i++)
   {
       for(j=1;j<=m;j++)
       {
           t[i][j]=min3(t[i-1][j],t[i][j-1],t[i-1][j-1])+cost[i][j];
       }
   }
   return t[m][n];
}
int main()
{
    int m,n;
    cin>>m>>n;
    int i,j;
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
            cin>>cost[i][j];
        }
    }

    cout<<minCost(m-1,n-1)<<endl;
}
